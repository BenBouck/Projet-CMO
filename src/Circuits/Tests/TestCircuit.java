package Circuits.Tests;

import Circuits.Circuit;
import Composants.*;

import java.util.List;

/** Pas propre du tout, mais test OK **/
public class TestCircuit {

    private static void test(Circuit circ)
    {
        List<Interrupteur> entrees = circ.getInputs();
        List<Vanne> sorties = circ.getOutputs();

        System.out.println("Nomenclautre : ");
        for(String s : circ.nomenclature())
            System.out.println(s);

        System.out.println();
        System.out.println("Description :");
        System.out.print(circ.description());

        System.out.println();
        System.out.println("Etats :");
        System.out.print(circ.traceEtats() + "\n");

        System.out.println("Entrées : ");
        for(Interrupteur i : entrees)
            System.out.println(i.description());
        System.out.println();

        System.out.println("Sorites : ");
        for(Vanne v : sorties)
            System.out.println(v.description());
        System.out.println();

        entrees.get(1).off();
        System.out.println("Etats (interrupteur de sécurité à 0):");
        System.out.print(circ.traceEtats() + "\n");
    }

    public static void main(String[] argv) {
        Interrupteur i1 = new Interrupteur(true);
        Interrupteur i2 = new Interrupteur(false);
        Interrupteur s = new Interrupteur(true);
        Or o = new Or(i1, i2);
        Not n = new Not(s);
        And a = new And(o, n);
        Vanne v = new Vanne(a);
        Composant[] c = new Composant[]{i1, i2, s, o, n, a, v};
        Circuit circ = new Circuit("Circuit test", c);

        test(circ);
    }
}
