package Circuits;

import Builder.Buildable;
import Composants.Composant;
import Composants.Interrupteur;
import Composants.Vanne;

import java.util.List;

public class Circuit implements Buildable {
    //region Variables
    private ListeComposants composants;
    private String nom;
    //endregion

    //region Constructeurs
    public Circuit(String nom, Composant[] cps) {
        this.nom = nom;
        composants = new ListeComposants(cps);
    }

    public Circuit(String nom) {
        this.nom = nom;
        composants = new ListeComposants();
    }

    public Circuit()
    {
        this("Sans nom");
    }
    //endregion

    //region Accesseurs
    public void setListeComposants(List<Composant> cps) {
        composants.addAll(cps);
    }
    //endregion

    //region Méthodes
    public List<String> nomenclature() {
        return composants.getIds();
    }

    public String description() {
        return this.nom + "\n" + composants.descriptions();
    }

    public String traceEtats() {
        return this.nom + "\n" + composants.traceEtats();
    }

    public List<Interrupteur> getInputs() {
        return composants.inputs();
    }

    public List<Vanne> getOutputs() {
        return composants.outputs();
    }
    //endregion

    //region Interface Buildable
    @Override
    public boolean estComposant() {
        return false;
    }
    //endregion
}
