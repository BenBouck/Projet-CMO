package Circuits;

import Composants.Composant;
import Composants.Interrupteur;
import Composants.Vanne;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/** ArrayList custom qui sait gérer des composants **/
public class ListeComposants extends ArrayList<Composant> {
    //region Constructeurs
    public ListeComposants(Composant [] cps) {
        super();
        this.addAll(Arrays.asList(cps));
        Collections.sort(this);
    }

    public ListeComposants() {
        super();
    }
    //endregion

    //region Méthodes
    public List<String> getIds() {
        List<String> ls = new ArrayList<>();
        for(Composant c : this)
            ls.add(c.getId());
        return ls;
    }

    public String descriptions() {
        String r = "";
        for (Composant c : this) {
            r += c.description() + "\n";    //Je n'ai pas compris pourquoi un Warning ici, apparamment Java n'aime pas les concaténations dans les boucles ?
        }
        return r;
    }

    public String traceEtats() {
        String r ="";
        for (Composant c : this) {
            r += c.traceEtat() + "\n";      //Je n'ai pas compris pourquoi un Warning ici, apparamment Java n'aime pas les concaténations dans les boucles ?
        }
        return r;
    }

    public List<Interrupteur> inputs() {
        List<Interrupteur> r = new ArrayList<>();
        for(Composant c : this)
        {
            if(c.isInput()) r.add((Interrupteur)c);
        }
        return r;
    }

    public List<Vanne> outputs() {
        List<Vanne> r = new ArrayList<>();
        for(Composant c : this)
        {
            if(c.isOutput()) r.add((Vanne)c);
        }
        return r;
    }
    //endregion
}
