package Commands;

import java.util.Scanner;

/** Scanner custom qui empeche de retourner 'null' pour une ligne vide **/
public class CommandScanner {
    //region Variables
    private final Scanner sc;
    //endregion

    //region Constructeurs
    public CommandScanner() {
        sc = new Scanner(System.in);
    }
    //endregion

    //region Methodes
    public String getLine() {
        String ret;
        try {
            ret = sc.nextLine();
        }
        catch (NullPointerException e) {
            ret = getLine();
        }
        return ret;
    }
    //endregion
}
