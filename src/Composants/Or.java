package Composants;

import Evaluable.CircuitInvalideException;
import Evaluable.NullSignalException;
import Signaux.Signal;

public class Or extends Porte2Entrees {
    //region Constructeurs
    public Or(Composant in1, Composant in2) {
        super(in1, in2);
    }

    public Or(Composant in1) {
        super(in1);
    }

    public Or() {
    }
    //endregion

    //region Methodes
    @Override
    public Signal evaluate() throws CircuitInvalideException {
        try {
            return getIn1().evaluate().or(getIn2().evaluate());
        }
        catch (NullSignalException e) {
            throw new CircuitInvalideException(this.description(), e);
        }
    }
    //endregion
}
