package Composants;

import Evaluable.CircuitInvalideException;
import Evaluable.NullSignalException;
import Signaux.Signal;

public class And extends Porte2Entrees {
    //region Constructeurs
    public And(Composant in1, Composant in2) {
        super(in1, in2);
    }

    public And(Composant in1) {
        super(in1);
    }

    public And() {
    }
    //endregion

    //region Methodes
    @Override
    public Signal evaluate() throws CircuitInvalideException {
        try {
            return getIn1().evaluate().and(getIn2().evaluate());
        }
        catch (NullSignalException e) {
            throw new CircuitInvalideException(this.description(), e);
        }
    }
    //endregion
}
