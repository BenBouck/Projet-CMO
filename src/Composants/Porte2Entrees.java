package Composants;

public abstract class Porte2Entrees extends Porte {
    //region Variables d'instance
    private Composant in1;
    private Composant in2;
    //endregion

    //region Constructeurs
    public Porte2Entrees(Composant in1, Composant in2) {
        this.in1 = in1;
        this.in2 = in2;
    }

    public Porte2Entrees(Composant in1) {
        this(in1, new NullComposant());
    }

    public Porte2Entrees() {
        this(new NullComposant(), new NullComposant());
    }
    //endregion

    //region Accesseurs
    public void setIn1(Composant in1) {
        this.in1 = in1;
    }

    public void setIn2(Composant in2) {
        this.in2 = in2;
    }

    public Composant getIn1() {
        return in1;
    }

    public Composant getIn2() {
        return in2;
    }
    //endregion

    //region Methodes

    @Override
    public String description() {
        return super.description() + "\n\tin1 : " + in1.getId() + "\n\tin2 : " + in2.getId();
    }

    //endregion
}
