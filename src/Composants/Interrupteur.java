package Composants;

import Signaux.False;
import Signaux.Signal;
import Signaux.True;

public class Interrupteur extends Composant {
    //region Variables d'instance
    private Signal etat;
    //endregion

    //region Constructeurs
    public Interrupteur(boolean etat)
    {
        this.etat = (etat)?new True():new False();
    }

    public Interrupteur()
    {
        this(false);
    }
    //endregion

    //region Methodes
    public void on() {
        this.etat=new True();
    }

    public void off() {
        this.etat=new False();
    }

    @Override
    public boolean isInput() {
        return true;
    }

    @Override
    public Signal evaluate() {
        return this.etat;
    }

    //endregion
}
