package Composants;

import Evaluable.NullSignalException;
import Signaux.Signal;

public class NullComposant extends Composant {
    //region Methodes
    @Override
    public String getId() {
        return "non connecte";
    }

    @Override
    public String description() {
        return "non connecte";
    }

    @Override
    public Signal evaluate() throws NullSignalException {
        throw new NullSignalException();
    }

    //endregion
}
