package Composants;

import Evaluable.CircuitInvalideException;
import Evaluable.NullSignalException;
import Signaux.Signal;

public class Vanne extends Composant {
    //region Variables d'instance
    private Composant in;
    //endregion

    //region Constructeurs
    public Vanne(Composant c)
    {
        this.in = c;
    }

    public Vanne()
    {
        this(new NullComposant());
    }
    //endregion

    //region Accesseurs
    public void setIn(Composant in) {
        this.in = in;
    }

    public Composant getIn() {
        return this.in;
    }
    //endregion

    //region Methodes
    @Override
    public String description() {
        return super.description() + "\n\tin : " + in.getId();
    }

    @Override
    public Signal evaluate() throws CircuitInvalideException {
        try {
            return in.evaluate();
        }
        catch (NullSignalException e) {
            throw new CircuitInvalideException(this.description(), e);
        }
    }

    @Override
    public boolean isOutput() {
        return true;
    }

    //endregion
}
