package Composants.Tests;

import Composants.*;

import java.util.ArrayList;

public class TestCircuit {
    public static void traceEtats(ArrayList<Composant> composants) {
        for(Composant c:composants) {
            try {
                System.out.print(c.description() + "\nEtat : " + c.evaluate().toString() + "\n\n\n");
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] argv) {
        //Instanciation des composants
        Interrupteur i1 = new Interrupteur(true);
        Interrupteur i2 = new Interrupteur(false);
        Interrupteur s = new Interrupteur(true);
        Or o = new Or(i1, i2);
        Not n = new Not(s);
        And a = new And(o, n);
        Vanne v = new Vanne(a);

        //Mise dans la liste
        ArrayList<Composant> composants = new ArrayList<>();
        composants.add(i1);
        composants.add(i2);
        composants.add(s);
        composants.add(o);
        composants.add(n);
        composants.add(a);
        composants.add(v);

        //Affichage
        traceEtats(composants);
    }
}
