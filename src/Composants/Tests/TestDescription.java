package Composants.Tests;

import Composants.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestDescription {
    //region Interrupteur
    @Test
    public void testInterrupteur() {
        Composant c = new Interrupteur();
        assertEquals(c.getId(), c.description());
    }
    //endregion

    //region Vanne
    @Test
    public void testVanneNC() {
        Composant c = new Vanne();
        assertEquals(c.getId() + "\n\tin : non connecte", c.description());
    }

    @Test
    public void testVanneC() {
        Vanne v = new Vanne(new Interrupteur());
        assertEquals(v.getId() + "\n\tin : " + v.getIn().getId(), v.description());
    }
    //endregion

    //region Not
    @Test
    public void testNotNC() {
        Not n = new Not();
        assertEquals(n.getId() + "\n\tin : non connecte", n.description());
    }

    @Test
    public void testNotC() {
        Not n = new Not(new Interrupteur());
        assertEquals(n.getId() + "\n\tin : " + n.getIn().getId(), n.description());
    }
    //endregion

    //region And
    @Test
    public void testAndNCNC() {
        And a = new And();
        assertEquals(a.getId()+"\n\tin1 : non connecte\n\tin2 : non connecte", a.description());
    }
    
    @Test
    public void testAndCNC() {
        And a = new And(new Interrupteur());
        assertEquals(a.getId() + "\n\tin1 : "+a.getIn1().getId() + "\n\tin2 : non connecte", a.description());
    }

    @Test
    public void testAndNCC() {
        And a = new And(new NullComposant(), new Not());
        assertEquals(a.getId() + "\n\tin1 : non connecte\n\tin2 : " + a.getIn2().getId(), a.description());
    }

    @Test
    public void TestAndCC() {
        And a = new And(new Interrupteur(), new Not());
        assertEquals(a.getId() + "\n\tin1 : " + a.getIn1().getId() + "\n\tin2 : " + a.getIn2().getId(), a.description());
    }
    //endregion

    //region Or
    @Test
    public void testOrNCNC() {
        Or a = new Or();
        assertEquals(a.getId()+"\n\tin1 : non connecte\n\tin2 : non connecte", a.description());
    }

    @Test
    public void testOrCNC() {
        Or a = new Or(new Interrupteur());
        assertEquals(a.getId() + "\n\tin1 : "+a.getIn1().getId() + "\n\tin2 : non connecte", a.description());
    }

    @Test
    public void testOrNCC() {
        Or a = new Or(new NullComposant(), new Not());
        assertEquals(a.getId() + "\n\tin1 : non connecte\n\tin2 : " + a.getIn2().getId(), a.description());
    }

    @Test
    public void TestOrCC() {
        Or a = new Or(new Interrupteur(), new Not());
        assertEquals(a.getId() + "\n\tin1 : " + a.getIn1().getId() + "\n\tin2 : " + a.getIn2().getId(), a.description());
    }
    //endregion
}
