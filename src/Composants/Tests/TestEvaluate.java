package Composants.Tests;

import Composants.*;
import Evaluable.CircuitInvalideException;
import Evaluable.NullSignalException;
import Signaux.False;
import Signaux.True;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class TestEvaluate {
    Composant c;

    //region Interrupteur
    @Test
    public void testInterrupteur1_0() {
        c = new Interrupteur(true);
        try {
            assertEquals(True.class, c.evaluate().getClass());
            ((Interrupteur) c).off();
            assertEquals(False.class, c.evaluate().getClass());
        }
        catch (Exception e) {
            fail(Arrays.toString(e.getStackTrace()));
        }

    }

    @Test
    public void testInterrupteur0_1() {
        c = new Interrupteur(false);
        try {
            assertEquals(False.class, c.evaluate().getClass());
            ((Interrupteur) c).on();
            assertEquals(True.class, c.evaluate().getClass());
        }
        catch (Exception e) {
            fail(Arrays.toString(e.getStackTrace()));
        }
    }
    //endregion

    //region NullComposant
    @Test
    public void testNullComposant() {
        c = new NullComposant();
        assertThrows(NullSignalException.class, ()-> c.evaluate());
    }
    //endregion

    //region Vanne
    @Test
    public void testVanne0() {
        c = new Vanne(new Interrupteur(false));
        try {
            assertEquals(False.class, c.evaluate().getClass());
        }
        catch (Exception e) {
            fail(Arrays.toString(e.getStackTrace()));
        }
    }

    @Test
    public void testVanne1() {
        c=new Vanne(new Interrupteur(true));
        try {
            assertEquals(True.class, c.evaluate().getClass());
        }
        catch (Exception e) {
            fail(Arrays.toString(e.getStackTrace()));
        }
    }

    @Test
    public void testVanneNull() {
        c=new Vanne();
        assertThrows(CircuitInvalideException.class, ()-> c.evaluate());
    }
    //endregion

    //region Not
    @Test
    public void testNot0() {
        c = new Not(new Interrupteur(false));
        try {
            assertEquals(True.class, c.evaluate().getClass());
        }
        catch (Exception e) {
            fail(Arrays.toString(e.getStackTrace()));
        }
    }

    @Test
    public void TestNot1() {
        c = new Not(new Interrupteur(true));
        try {
            assertEquals(False.class, c.evaluate().getClass());
        }
        catch (Exception e) {
            fail(Arrays.toString(e.getStackTrace()));
        }
    }

    @Test
    public void TestNotNull() {
        c=new Not();
        assertThrows(CircuitInvalideException.class, ()-> c.evaluate());
    }
    //endregion

    //region And
    @Test
    public void TestAnd00() {
        c = new And(new Interrupteur(false), new Interrupteur(false));
        try {
            assertEquals(False.class, c.evaluate().getClass());
        }
        catch (Exception e) {
            fail(Arrays.toString(e.getStackTrace()));
        }
    }

    @Test
    public void TestAnd01() {
        c = new And(new Interrupteur(false), new Interrupteur(true));
        try {
            assertEquals(False.class, c.evaluate().getClass());
        }
        catch (Exception e) {
            fail(Arrays.toString(e.getStackTrace()));
        }
    }

    @Test
    public void TestAnd10() {
        c = new And(new Interrupteur(true), new Interrupteur(false));
        try {
            assertEquals(False.class, c.evaluate().getClass());
        }
        catch (Exception e) {
            fail(Arrays.toString(e.getStackTrace()));
        }
    }

    @Test
    public void TestAnd11() {
        c = new And(new Interrupteur(true), new Interrupteur(true));
        try {
            assertEquals(True.class, c.evaluate().getClass());
        }
        catch (Exception e) {
            fail(Arrays.toString(e.getStackTrace()));
        }
    }

    @Test
    public void TestAndNull() {
        c=new And();
        assertThrows(CircuitInvalideException.class, ()-> c.evaluate());
    }
    //endregion

    //region Or
    @Test
    public void TestOr00() {
        c = new Or(new Interrupteur(false), new Interrupteur(false));
        try {
            assertEquals(False.class, c.evaluate().getClass());
        }
        catch (Exception e) {
            fail(Arrays.toString(e.getStackTrace()));
        }
    }

    @Test
    public void TestOr01() {
        c = new Or(new Interrupteur(false), new Interrupteur(true));
        try {
            assertEquals(True.class, c.evaluate().getClass());
        }
        catch (Exception e) {
            fail(Arrays.toString(e.getStackTrace()));
        }
    }

    @Test
    public void TestOr10() {
        c = new Or(new Interrupteur(true), new Interrupteur(false));
        try {
            assertEquals(True.class, c.evaluate().getClass());
        }
        catch (Exception e) {
            fail(Arrays.toString(e.getStackTrace()));
        }
    }

    @Test
    public void TestOr11() {
        c = new Or(new Interrupteur(true), new Interrupteur(true));
        try {
            assertEquals(True.class, c.evaluate().getClass());
        }
        catch (Exception e) {
            fail(Arrays.toString(e.getStackTrace()));
        }
    }

    @Test
    public void TestOrNull() {
        c=new Or();
        assertThrows(CircuitInvalideException.class, ()-> c.evaluate());
    }
    //endregion
}
