package Composants;

public class ComposantInvalideException extends Exception{
    public ComposantInvalideException(String message, Throwable cause) {
        super(message, cause);
    }
}
