package Composants;

import Evaluable.CircuitInvalideException;
import Evaluable.NullSignalException;
import Signaux.Signal;

public class Not extends Porte {
    //region Variables d'instance
    private Composant in;
    //endregion

    //region Constructeurs
    public Not(Composant in)
    {
        this.in = in;
    }

    public Not() {
        this(new NullComposant());
    }
    //endregion

    //region Accesseurs
    public void setIn(Composant in) {
        this.in = in;
    }

    public Composant getIn() {
        return in;
    }
    //endregion

    //region Methodes

    @Override
    public String description() {
        return super.description() + "\n\tin : " + in.getId();
    }

    @Override
    public Signal evaluate() throws CircuitInvalideException {
        try {
            return in.evaluate().not();
        }
        catch (NullSignalException e) {
            throw new CircuitInvalideException(this.description(), e);
        }
    }

    //endregion
}