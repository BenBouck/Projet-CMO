package Composants;

import Builder.Buildable;
import Evaluable.CircuitInvalideException;
import Evaluable.Evaluable;
import Evaluable.NullSignalException;
import Signaux.Signal;

public abstract class Composant implements Comparable<Composant>, Evaluable, Buildable {
    //region Methodes
    public String getId()
    {
        return super.toString();
    }

    public String description()
    {
        return getId();
    }

    public boolean isInput() {
        return false;
    }

    public boolean isOutput() {
        return false;
    }

    public String traceEtat() {
        String r = getId() + " ";
        try {
            r += this.evaluate().toString();
        }
        catch (Exception e)
        {
            r += "Erreur";
        }
        return r;
    }
    //endregion

    //region Interface Comparable
    public int compareTo(Composant c) {
        return this.getId().compareTo(c.getId());
    }
    //endregion

    //region Interface Evaluable
    public abstract Signal evaluate() throws NullSignalException, CircuitInvalideException;
    //endregion

    //region Interface Buildable
    @Override
    public boolean estComposant() {
        return true;
    }
    //endregion
}
