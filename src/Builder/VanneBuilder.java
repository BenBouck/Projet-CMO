package Builder;

import Composants.Composant;
import Composants.Vanne;

import java.util.HashMap;

public class VanneBuilder extends ElementBuilder{
    @Override
    public Buildable build(String[] command, HashMap<String, Buildable> mapElements) throws UnknownBuildableException {
        Composant in;
        String sIn;

        /* Récupération du nom de composant d'entrée */
        try {
            sIn = command[2];
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new UnknownBuildableException("Trop peu d'arguments (donné 2, nécessite 3).", e);
        }

        /* Récupération du composant d'entrée */
        try {
            if ((in = (Composant) mapElements.get(sIn)) == null)
                throw new NullPointerException();
        } catch (NullPointerException e) {
            throw new UnknownBuildableException("Element \"" + sIn + "\" inconnu.", e);
        } catch (ClassCastException e) {
            throw new UnknownBuildableException("Element \"" + sIn + "\" de mauvais type", e);
        }

        return new Vanne(in);
    }
}
