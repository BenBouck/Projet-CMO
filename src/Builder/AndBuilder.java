package Builder;

import Composants.And;
import Composants.Composant;

import java.util.HashMap;

/** Sert à crée un nouveau And **/
public class AndBuilder extends ElementBuilder {
    @Override
    /* Méthode de construction d'un 'And' */
    public Buildable build(String[] command, HashMap<String,Buildable> mapElements) throws UnknownBuildableException {
        Composant in1, in2;
        String sIn1, sIn2;

        /* Récupère le nom du composant n°1 */
        try {
            sIn1 = command[2];
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new UnknownBuildableException("Trop peu d'arguments (donné 2, nécessite 4).", e);
        }

        /* Récupère le nom du composant n°2 */
        try {
            sIn2 = command[3];
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new UnknownBuildableException("Trop peu d'arguments (donné 3, nécessite 4).", e);
        }

        /* Récupère le composant n°1 */
        try {
            if ((in1 = (Composant) mapElements.get(sIn1)) == null)
                throw new NullPointerException();
        } catch (NullPointerException e) {
            throw new UnknownBuildableException("Element \"" + sIn1 + "\" inconnu.", e);
        } catch (ClassCastException e) {
            throw new UnknownBuildableException("Element \"" + sIn1 + "\" de mauvais type", e); //Sécurité si package réutilisé hors de mon programme
        }

        /* Récupère le composant n°2 */
        try {
            if ((in2 = (Composant) mapElements.get(sIn2)) == null)
                throw new NullPointerException();
        } catch (NullPointerException e) {
            throw new UnknownBuildableException("Element \"" + sIn2 + "\" inconnu.", e);
        } catch (ClassCastException e) {
            throw new UnknownBuildableException("Element \"" + sIn2 + "\" de mauvais type", e);
        }

        /* Crée le composant */
        return new And(in1, in2);
    }
}
