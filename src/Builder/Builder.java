package Builder;

import Circuits.Circuit;
import Commands.CommandScanner;
import Composants.Composant;

import java.util.HashMap;

/** Procédure de saisie d'un composant au clavier, avec sauvegarde de la liste des composants.
 ** Bouclage à réaliser dans une fonction spécifique.
 **/
public class Builder {
    private final HashMap<String, ElementBuilder> builderMap;
    private final HashMap<String, Buildable> elementsMap;
    private final CommandScanner scanCmd;

    //region Constructeur
    public Builder() {
        builderMap = initBuilderMap();
        elementsMap = new HashMap<>();
        scanCmd = new CommandScanner();
    }

    private HashMap<String, ElementBuilder> initBuilderMap() {
        HashMap<String, ElementBuilder> builderMap;
        builderMap = new HashMap<>();
        builderMap.put("and", new AndBuilder());
        builderMap.put("or", new OrBuilder());
        builderMap.put("not", new NotBuilder());
        builderMap.put("interrupteur", new InterrupteurBuilder());
        builderMap.put("vanne", new VanneBuilder());
        builderMap.put("circuit", new CircuitBuilder());
        return builderMap;
    }
    //endregion

    //region Méthodes
    public Buildable promptBuildable() {
        System.out.print(">> ");
        Buildable ret;
        String[] data = scanCmd.getLine().split(" ");

        /* Façon de faire médiocre. Amélioration possible :
         * Créer une classe qui lit un ensemble de commandes (ls, help, build/new) et qui appelle builder
         * le cas échéant.
         */
        if(data[0].compareTo("help") == 0)
            return promptAide();

        if(data[0].compareTo("ls") == 0)
            return promptElements();

        /* Construit le composant */
        try {
            ret = builderMap.get(data[0]).build(data, elementsMap);
        } catch (UnknownBuildableException e) {
            promptErrMess(e.getMessage());
            return promptBuildable();
        } catch (NullPointerException e) {
            return promptBuildable();           //Permet de faire des sauts de lignes
        }

        /* Ajout du composant nommé dans la hashmap */
        try {
            elementsMap.put(data[1], ret);
        } catch (ArrayIndexOutOfBoundsException e) {
            promptErrMess("Trop peu d'arguments.");
        }

        return ret;
    }

    /* Affichage d'un message d'erreur formaté */
    private void promptErrMess(String message) {
        System.out.println("Erreur :");
        System.out.println(message);
        System.out.println("Utilisation : <type> <nom> [input 1] [input 2]");
        System.out.println("Aide : >> help");
    }

    /* Affichage de l'aide */
    private Buildable promptAide() {
        promptErrMess("Aide :");
        System.out.println("Types possibles : ");
        for(String elt : builderMap.keySet()) {
            System.out.println("\t"+elt);
        }
        System.out.println("Liste des elements : >> ls");
        return promptBuildable();
    }

    /* Affichage des éléments */
    private Buildable promptElements() {
        for(String elt : elementsMap.keySet()) {
            if(elementsMap.get(elt) instanceof Composant) {
                System.out.println(elt + " : " + ((Composant) elementsMap.get(elt)).description());
            } else if (elementsMap.get(elt) instanceof Circuit) {
                System.out.println(elt + " : " + ((Circuit) elementsMap.get(elt)).description());
            }
        }
        return promptBuildable();
    }
    //endregion
}
