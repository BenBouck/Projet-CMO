package Builder;

import Composants.Composant;
import Composants.Or;

import java.util.HashMap;

public class OrBuilder extends ElementBuilder{
    public OrBuilder() {
        super();
    }

    @Override
    public Buildable build(String[] command, HashMap<String, Buildable> mapElements) throws UnknownBuildableException {
        Composant in1, in2;
        String sIn1, sIn2;

        /* Récupération du nom de composant n°1 */
        try {
            sIn1 = command[2];
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new UnknownBuildableException("Trop peu d'arguments (donné 2, nécessite 4).", e);
        }

        /* Récupération du nom de composant n°2 */
        try {
            sIn2 = command[3];
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new UnknownBuildableException("Trop peu d'arguments (donné 3, nécessite 4).", e);
        }

        /* Récupération du composant n°1 */
        try {
            if ((in1 = (Composant) mapElements.get(sIn1)) == null)
                throw new NullPointerException();
        } catch (NullPointerException e) {
            throw new UnknownBuildableException("Element \"" + sIn1 + "\" inconnu.", e);
        } catch (ClassCastException e) {
            throw new UnknownBuildableException("Element \"" + sIn1 + "\" de mauvais type.", e);
        }

        /* Récupération du composant n°2 */
        try {
            if ((in2 = (Composant) mapElements.get(sIn2)) == null)
                throw new NullPointerException();
        } catch (NullPointerException e) {
            throw new UnknownBuildableException("Element \"" + sIn2 + "\" inconnu.", e);
        } catch (ClassCastException e) {
            throw new UnknownBuildableException("Element \"" + sIn2 + "\" de mauvais type.", e);
        }

        return new Or(in1, in2);
    }
}
