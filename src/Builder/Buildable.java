package Builder;

/** Sert à rendre compatible les circuits et les composants pour ElementBuilder **/
public interface Buildable {
    /* Renvoie 'true' si il s'agit d'un composant, 'false' sinon */
    boolean estComposant();
}
