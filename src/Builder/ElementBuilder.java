package Builder;

import java.util.HashMap;

/** Classe abstraite de constructeurs de composants **/
public abstract class ElementBuilder{
    public abstract Buildable build(String[] command, HashMap<String, Buildable> mapElements) throws UnknownBuildableException;
}
