package Builder;

/** Exception d'erreur de construction de composant **/
public class UnknownBuildableException extends Exception {
    public UnknownBuildableException(String message, Throwable cause) {
        super(message, cause);
    }
}
