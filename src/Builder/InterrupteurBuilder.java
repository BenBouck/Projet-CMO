package Builder;

import Composants.Interrupteur;

import java.util.HashMap;

public class InterrupteurBuilder extends ElementBuilder {
    @Override
    public Buildable build(String[] command, HashMap<String, Buildable> mapElements) {
        /* Crée un nouvel interrupteur (par défaut : état bas) */
        return new Interrupteur();
    }
}
