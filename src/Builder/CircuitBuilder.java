package Builder;

import Circuits.Circuit;
import Composants.Composant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CircuitBuilder extends ElementBuilder{
    @Override
    public Buildable build(String[] command, HashMap<String, Buildable> mapElements) throws UnknownBuildableException {
        String name;
        Circuit c;
        List<Composant> cps = new ArrayList<>();

        /* Récupère le nom du circuit */
        try {
            name = command[1];
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new UnknownBuildableException("Trop peu d'arguments (1 fourni, 2 attendus).", e);
        }

        /* Création d'un circuit */
        c = new Circuit(name);

        /* Récupération des composants de la liste d'éléments */
        for(Buildable b : mapElements.values())
            if(b.estComposant())
                cps.add((Composant) b);

        /* Ajout des composants dans le circuit */
        c.setListeComposants(cps);

        return c;
    }
}
