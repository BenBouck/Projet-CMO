package Simulation;

import Circuits.Circuit;

public class CmdHelp extends Commande{
    public CmdHelp(Circuit cir, Simulateur sender) {
        super(cir, sender);
    }

    @Override
    public boolean execute(String[] args) {
        System.out.println("Utilisation : <commande> [arguments]");
        System.out.println("Commandes :");
        System.out.println("\ti <n> [1|0]\t:\tPlace l'interrupteur n dans l'etat donne ou l'inverse");
        System.out.println("\to <n>\t\t:\tAffiche l'etat de la sortie n.");
        System.out.println("\tls\t\t\t:\tAffiche la nomenclauture");
        System.out.println("\tll\t\t\t:\tAffiche le detail du circuit");
        System.out.println("\ttrace\t\t:\tAffiche l'etat de chaque composant");
        System.out.println("\tins\t\t\t:\tAffiche les entrees");
        System.out.println("\touts\t\t:\tAffiche les sorties\n");
        System.out.println("\texit\t\t:\tSort du programme");
        return super.sender.commandPrompt();
    }

    @Override
    public String explication() {
        return "";
    }
}
