package Simulation;

import Circuits.Circuit;

public class CmdLs extends Commande{
    public CmdLs(Circuit c, Simulateur sender) {
        super(c, sender);
    }

    @Override
    public boolean execute(String[] args) {
        System.out.println(c.nomenclature());
        return true;
    }

    @Override
    public String explication() {
        return "";
    }
}
