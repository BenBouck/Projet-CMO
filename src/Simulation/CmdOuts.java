package Simulation;

import Circuits.Circuit;
import Composants.Vanne;

public class CmdOuts extends Commande {
    public CmdOuts(Circuit c, Simulateur sender) {
        super(c, sender);
    }

    @Override
    public boolean execute(String[] args) {
        int i = 0;
        for(Vanne out : c.getOutputs())
            System.out.println("(" +(i++) + ") : " + out.description());
        return true;
    }

    @Override
    public String explication() {
        return "";
    }
}
