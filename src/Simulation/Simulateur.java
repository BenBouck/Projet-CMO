package Simulation;

import Circuits.Circuit;
import Commands.CommandScanner;

import java.util.HashMap;

public class Simulateur {
    private Circuit cir;
    private CommandScanner scanner;
    private HashMap<String, Commande> cmdMap;

    public Simulateur(Circuit cir) {
        this.cir = cir;
        this.scanner = new CommandScanner();
        cmdMap = this.initCmdMap();
    }

    private HashMap<String, Commande> initCmdMap() {
        HashMap<String, Commande> map = new HashMap<>();
        map.put("help", new CmdHelp(this.cir, this));
        map.put("", new CmdNull(this));
        map.put("exit", new CmdExit(this));
        map.put("i", new CmdSetInput(cir, this));
        map.put("o", new CmdGetOutput(cir, this));
        map.put("ls", new CmdLs(cir, this));
        map.put("ll", new CmdLl(cir, this));
        map.put("ins", new CmdIns(cir, this));
        map.put("outs", new CmdOuts(cir, this));
        map.put("trace", new CmdTrace(cir, this));
        return map;
    }

    public boolean commandPrompt() {
        System.out.print(">> ");
        String[] commande = scanner.getLine().split(" ");

        try{
            return cmdMap.get(commande[0]).execute(commande);
        } catch (Exception e) {
            return errMsg();
        }
    }

    private boolean errMsg() {
        System.out.println("Erreur ");
        System.out.println("Utilisation : <commande> [arguments]");
        System.out.println("Aide : help");
        return commandPrompt();
    }
}
