package Simulation;

import Circuits.Circuit;

public class CmdTrace extends Commande {
    public CmdTrace(Circuit c, Simulateur sender) {
        super(c, sender);
    }

    @Override
    public boolean execute(String[] args) {
        System.out.println(c.traceEtats());
        return true;
    }

    @Override
    public String explication() {
        return "";
    }
}
