package Simulation;

import Circuits.Circuit;

/** Classe de commandes pour le simulateur **/
public abstract class Commande {
    protected Circuit c;
    protected Simulateur sender;
    public Commande(Circuit c, Simulateur sender) {
        this.c = c;
        this.sender = sender;
    }

    public abstract boolean execute(String[] args);
    public abstract String explication();

    public boolean messageErreur(String message) {
        System.out.println(message);
        System.out.println("Utilisation : " + explication());
        return sender.commandPrompt();
    }
}
