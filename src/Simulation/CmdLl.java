package Simulation;

import Circuits.Circuit;

public class CmdLl extends Commande{
    public CmdLl(Circuit c, Simulateur sender) {
        super(c, sender);
    }

    @Override
    public boolean execute(String[] args) {
        System.out.println(c.description());
        return true;
    }

    @Override
    public String explication() {
        return "";
    }
}
