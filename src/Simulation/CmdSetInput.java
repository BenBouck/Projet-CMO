package Simulation;

import Circuits.Circuit;
import Composants.Interrupteur;

import java.util.List;

import static java.lang.Integer.parseInt;

public class CmdSetInput extends Commande{

    public CmdSetInput(Circuit c, Simulateur sender) {
        super(c, sender);
    }

    @Override
    public boolean execute(String[] args) {
        List<Interrupteur> ins = c.getInputs();
        Interrupteur in;
        int etatVoulu, noInterrupteur;

        try {
            noInterrupteur = parseInt(args[1]);
        } catch (ArrayIndexOutOfBoundsException e) {
            return super.messageErreur("Trop peu d'arguments");
        } catch (Exception e) {
            return super.messageErreur("Erreur argument : " + args[1]);
        }

        try {
            in = ins.get(noInterrupteur);
        } catch (Exception e) {
            return super.messageErreur("Erreur argument : " + args[1]);
        }

        try {
            etatVoulu = parseInt(args[2]);
        } catch (ArrayIndexOutOfBoundsException e) {
            if(in.evaluate().toString().compareTo("True") == 0) {
                in.off();
            } else {
                in.on();
            }
            return true;
        } catch (Exception e) {
            return super.messageErreur("Argument invalide : " + args[2]);
        }

        if(etatVoulu == 0)
            in.off();
        else if(etatVoulu == 1)
            in.on();
        else {
            return super.messageErreur("Argument invalide : " + args[2]);
        }

        return true;
    }

    @Override
    public String explication() {
        return "i <n> [0|1] : place l'entrée n°<i> dans l'état spécifié, ou l'inverse.";
    }
}
