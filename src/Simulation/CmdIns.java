package Simulation;

import Circuits.Circuit;
import Composants.Interrupteur;

public class CmdIns extends Commande{
    public CmdIns(Circuit c, Simulateur sender) {
        super(c, sender);
    }

    @Override
    public boolean execute(String[] args) {
        int i = 0;
        for(Interrupteur in : c.getInputs())
            System.out.println("(" +(i++) + ") : " + in.description());
        return true;
    }

    @Override
    public String explication() {
        return "";
    }
}
