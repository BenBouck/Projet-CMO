package Simulation;

import Circuits.Circuit;
import Composants.Vanne;

import java.util.List;

import static java.lang.Integer.parseInt;

public class CmdGetOutput extends Commande{
    public CmdGetOutput(Circuit c, Simulateur sender) {
        super(c, sender);
    }

    @Override
    public boolean execute(String[] args) {
        List<Vanne> outs = c.getOutputs();
        Vanne out;
        int noVanne;

        try {
            noVanne = parseInt(args[1]);
        } catch (ArrayIndexOutOfBoundsException e) {
            return messageErreur("Trop peu d'arguments.\nUtilisation : >> o <n>");
        } catch (Exception e) {
            return messageErreur("Argument invalide : ");
        }

        try {
            out = outs.get(noVanne);
        } catch (Exception e) {
            return messageErreur("Argument invalide : " + args[1]);
        }

        System.out.println(out.traceEtat());
        return true;
    }

    @Override
    public String explication() {
        return "o <n> : affiche l'état de la vanne n°<n>.";
    }
}
