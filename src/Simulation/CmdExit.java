package Simulation;

public class CmdExit extends Commande {

    public CmdExit(Simulateur sender) {
        super(null, sender);
    }

    @Override
    public boolean execute(String[] args) {
        return false;
    }

    @Override
    public String explication() {
        return "";
    }
}
