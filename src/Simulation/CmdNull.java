package Simulation;

public class CmdNull extends Commande{
    public CmdNull(Simulateur sender) {
        super(null, sender);
    }

    @Override
    public boolean execute(String[] args) {
        return sender.commandPrompt();
    }

    @Override
    public String explication() {
        return "";
    }
}
