package Signaux.Tests;

import Signaux.False;
import Signaux.True;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestTrue {
    private True t;
    @BeforeEach
    public void init() {
        t = new True();
    }

    @Test
    public void value() {
        assertTrue(t.value());
    }

    @Test
    public void not() {
        assertEquals(False.class, t.not().getClass());
    }

    @Test
    public void and0() {
        assertEquals(False.class, t.and(new False()).getClass());
    }

    @Test
    public void and1() {
        assertEquals(True.class, t.and(new True()).getClass());
    }

    @Test
    public void or0() {
        assertEquals(True.class, t.or(new False()).getClass());
    }

    @Test
    public void or1() {
        assertEquals(True.class, t.or(new True()).getClass());
    }

    @Test
    public void _toString() {
        assertEquals("True", t.toString());
    }
}
