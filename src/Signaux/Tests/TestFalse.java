package Signaux.Tests;

import Signaux.False;
import Signaux.True;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class TestFalse {
    private False t;
    @BeforeEach
    public void init() {
        t = new False();
    }

    @Test
    public void value() {
        assertFalse(t.value());
    }

    @Test
    public void not() {
        assertEquals(True.class, t.not().getClass());
    }

    @Test
    public void and0() {
        assertEquals(False.class, t.and(new False()).getClass());
    }

    @Test
    public void and1() {
        assertEquals(False.class, t.and(new True()).getClass());
    }

    @Test
    public void or0() {
        assertEquals(False.class, t.or(new False()).getClass());
    }

    @Test
    public void or1() {
        assertEquals(True.class, t.or(new True()).getClass());
    }

    @Test
    public void _toString() {
        assertEquals("False", t.toString());
    }
}
