package Signaux;

public class False extends Signal{
    //region Constructeur
    public False() {
        super(false);
    }
    //endregion

    //region Méthodes
    @Override
    public Signal not() {
        return new True();
    }

    @Override
    public Signal and(Signal sig) {
        return this;
    }

    @Override
    public Signal or(Signal sig) {
        return sig;
    }

    @Override
    public String toString() {
        return "False";
    }
    //endregion
}
