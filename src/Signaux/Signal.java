package Signaux;

public abstract class Signal {
    //region Variables
    private boolean val;
    //endregion

    //region Constructeurs
    public Signal(boolean val) {
        this.val = val;
    }
    //endregion

    //region Méthodes
    public boolean value() {
        return this.val;
    }

    public abstract Signal not();
    public abstract Signal and(Signal sig);
    public abstract Signal or(Signal sig);
    @Override
    public abstract String toString();
    //endregion
}
