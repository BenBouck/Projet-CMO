package Signaux;

public class True extends Signal {
    //region Constructeur
    public True() {
        super(true);
    }
    //endregion

    //region Méthodes
    @Override
    public Signal not() {
        return new False();
    }

    @Override
    public Signal and(Signal sig) {
        return sig;
    }

    @Override
    public Signal or(Signal sig) {
        return this;
    }

    @Override
    public String toString() {
        return "True";
    }
    //endregion
}
