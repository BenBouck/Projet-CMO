package Evaluable;

public class CircuitInvalideException extends Exception {
    public CircuitInvalideException(String message, Throwable cause) {
        super(message, cause);
    }
}
