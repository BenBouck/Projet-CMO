package Evaluable;

import Signaux.Signal;

public interface Evaluable {
    Signal evaluate() throws NullSignalException, CircuitInvalideException;
}
