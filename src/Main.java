import Builder.Buildable;
import Builder.Builder;
import Circuits.Circuit;
import Simulation.Simulateur;

public class Main {
    private  static void simulation(Circuit c) {
        Simulateur sim = new Simulateur(c);
        while(sim.commandPrompt()); //While vide volontairement
    }

    private static Circuit initCircuit() {
        Builder b = new Builder();
        Buildable elt;

        while((elt=b.promptBuildable()).estComposant()); //While vide volontairement

        return (Circuit) elt;
    }

    public static void main(String[] argv) {
        Circuit cir;

        /** Etape 1 : Saisie du circuit **/
        System.out.println("Saisie des composants du circuit : ");
        cir = initCircuit();

        /** Etape 2 : Simulation du circuit **/
        System.out.println("Simulation du circuit : ");
        simulation(cir);

        System.out.println("Fin de la simulation.");
    }
}
