# Simulateur de circuits logiques
## Projet de CMO | Compte-rendu

Benoît Bouckaert

Ce projet a poue but de réaliser un simulateur de circuits logiques, en mettant en place de bonnes pratiques de
programmation orientée objet. Il est réalisé sous Java, en utilisant l'IDE [IntelliJ](https://www.jetbrains.com/fr-fr/idea/).

# I) Fonctionnement

## Interface de commandes

Afin de lancer ce projet, compiler l'intégralité des sources hors */Test, puis exécuter la classe `Main`.

### Création du circuit

La première phase de test est de créer le circuit à simuler. Pour cela, l'u commandes ont l'un des deux formats suivants :
```
>> <element> <nom> [<entree1> [entree 2]]>
>> <commande>
```

Le champ `element` peut prendre les valeurs suivantes :
- `interrupteur` : crée un interrupteur (à l'état bas)
- `not` : crée une porte non
- `and` : crée une porte et
- `or` : crée une porte ou
- `vanne` : crée une vanne
- `circuit` : crée un circuit avec tous les composants déclarés et arrête la phase de création.

Le champ `nom` prend le nom du composant, pour pourvoir utiliser ce dernier en entrée d'un autre composant.

Certains composants ont besoin d'autres composants en entrée : 
- `not` et `vanne` nécessitent chacun un composant en entrée
- `and` et `or` nécessitent chacun deux composants en entrée.

In est nécessaire que les composants utilisés en entrée aient été précédemment créés.

De plus, deux commandes sont admises :
- `help` : affiche une aide
- `ls` : affiche la liste des composants déjà créés

### Simulation du circuit

La phase de simulation du circuit se fait via les commandes suivantes :
- `ins` : affiche les entrées
- `outs` : affiche les sorties
- `i <n> [1|0]` : inverse l'état de l'entrée `n`, ou lui donne l'état spécifié
- `o <n>` : affiche l'état de la sortie `n`
- `trace` : trace les états du circuit
- `ls` : affiche la nomenclature du circuit
- `ll` : affiche les détails du circuit
- `help` : affiche une aide
- `exit` : sort de la simulation, arrête le programme

### Exemple d'exécution

```
Saisie des composants du circuit : 
>> interrupteur i0
>> interrupteur i1
>> interrupteur i2
>> or o0 i0 i1
>> not n0 i2
>> and a0 n0 o0
>> vanne v0
Erreur :
Trop peu d'arguments (donné 2, nécessite 3).
Utilisation : <type> <nom> [input 1] [input 2]
Aide : >> help
>> vanne v0 a0
>> circuit circuitTest
Simulation du circuit : 
>> ins
(0) : Composants.Interrupteur@3d494fbf
(1) : Composants.Interrupteur@3f91beef
(2) : Composants.Interrupteur@1a6c5a9e
>> outs
(0) : Composants.Vanne@37bba400
	in : Composants.And@179d3b25
>> o 0
Composants.Vanne@37bba400 False
>> trace
circuitTest
Composants.Or@4b9af9a9 False
Composants.Not@4459eb14 True
Composants.Interrupteur@3d494fbf False
Composants.Interrupteur@3f91beef False
Composants.Interrupteur@1a6c5a9e False
Composants.Vanne@37bba400 False
Composants.And@179d3b25 False

>> ls
[Composants.Or@4b9af9a9, Composants.Not@4459eb14, Composants.Interrupteur@3d494fbf, Composants.Interrupteur@3f91beef, Composants.Interrupteur@1a6c5a9e, Composants.Vanne@37bba400, Composants.And@179d3b25]
>> ll
circuitTest
Composants.Or@4b9af9a9
	in1 : Composants.Interrupteur@3d494fbf
	in2 : Composants.Interrupteur@3f91beef
Composants.Not@4459eb14
	in : Composants.Interrupteur@1a6c5a9e
Composants.Interrupteur@3d494fbf
Composants.Interrupteur@3f91beef
Composants.Interrupteur@1a6c5a9e
Composants.Vanne@37bba400
	in : Composants.And@179d3b25
Composants.And@179d3b25
	in1 : Composants.Not@4459eb14
	in2 : Composants.Or@4b9af9a9

>> i 0
>> trace
circuitTest
Composants.Or@4b9af9a9 True
Composants.Not@4459eb14 True
Composants.Interrupteur@3d494fbf True
Composants.Interrupteur@3f91beef False
Composants.Interrupteur@1a6c5a9e False
Composants.Vanne@37bba400 True
Composants.And@179d3b25 True

>> o 0
Composants.Vanne@37bba400 True
>> help
Utilisation : <commande> [arguments]
Commandes :
	i <n> [1|0]	:	Place l'interrupteur n dans l'etat donne ou l'inverse
	o <n>		:	Affiche l'etat de la sortie n.
	ls			:	Affiche la nomenclauture
	ll			:	Affiche le detail du circuit
	trace		:	Affiche l'etat de chaque composant
	ins			:	Affiche les entrees
	outs		:	Affiche les sorties

	exit		:	Sort du programme
>> exit
Fin de la simulation.

Process finished with exit code 0
```

# II) Description du code

Le projet est divisé en 6 packages.

## Composants

Ce package contient l'ensemble des classes de composants.

### Super-classe Composant

Cette classe sert de base à chaque composant. Il en découle les sous-classes suivantes :
- Interrupteur
- Vanne
- Porte
    - Not
    - Porte2Entrees
        - And
        - Or
- NullComposant

La classe NullComposant permet d'éviter les erreurs dues à des pointeurs nuls, en créant des exceptions personalisées.

Chaque composant sait répondre à l'ensemble des méthodes suivantes :
- `String getId()` : renvoie la classe et l'identifiant de l'objet
- `String description()` : renvoie la description du composant, à savoir son id, et les id de ses entrées
- `boolean isInput()` : indique si le composant est une entrée (interrupteur) ou non
- `boolean isOutput()` : indique si le composant est une sortie (vanne) ou non
- `String traceEtat()` : renvoie l'id du composant et son état

Cette classe implémente les interfaces Comparable, Evaluable (cf. ci-dessous) et Buildable (cf. ci-dessous).

### Exceptions

Deux types d'exceptions sont également créées dans ce package :
- `NonConnecteException`, une exception bas niveau, envoyée par un NullComposant lors de son évaluation
- `ComposantInvalideException`, une exception haut niveau, qui indique le lieu du dysfonctionnement

### Tests

Deux fichiers de tests unitaires sont inclus dans ce package, dans le but de tester les méthodes de description et 
d'évaluation des composants.

Un troisième fichier vise à tester une ébauche de circuit (sans la classe Circuit).

## Circuits

### Classe Circuit

La classe circuit vise à modéliser un circuit, caractérisé par un nom et une liste de composants. Elle répond aux 
messages suivants :
- `List<String> nomenclature()` : renvoie les id de tous les composants
- `String description()` : renvoie le nom du circuit suivi de la description de chaque composant
- `String traceEtats()` : trace les états de tous les composants
- `List<Interrupteur> getInputs()` : renvoie la liste des entrées du circuit
- `List<Vanne> getOutputs()` : renvoie la liste des sorties du circuit

### Classe ListeComposants

Cette classe étend la classe ArrayList<Composant>. Elle vise à donner le traitement itératif nécessaire aux différentes 
méthodes de Circuit à la liste directement.

## Signaux

Les signaux hauts et bas sont modélisés par des objets. Leur évaluation est permise par l'interface Evaluable.

### Classe Signal

La classe signal vise à modéliser des signaux logiques. Elle possède deux sous-classes True et False, et permet de 
répondre auxc méthodes :
- `boolean Value()` : renvoie l'état logique du signal
- `Signal not()` : renvoie l'inverse du signal
- `Signal and(Signal bool)` : renvoie un et logique entre `this` et `bool`
- `Signal or(Signal bool)` : renvoie un ou logique entre `this` et `bool`
- `String toString()` : renvoie "True" ou "False"

### Interface Evaluable

L'interface evaluable vise à implémenter la méthode `Signal evaluate()` dans les composants.

## Commands

Afin de simplifier la lecture de chaînes de caractères, la classe `CommandScanner` vise à simlpifier l'utilisation de 
la classe `Java.util.Scanner`, en empêchant l'envoi de `NullPointerException`.

## Builder

Le package Builder vise à fournir une interface de création de circuits par saisie de commande, comme décrit dans la 
première partie.

Elle se base sur deux classes principales, et une interface.

### Classe Builder

La classe builder comporte une routine qui scanne une commande, et la traite. Elle retourne toujours un élément 
`Buildable`. C'est pour cela qu'en cas d'erreur de la part de l'utilisateur, la méthode `promptBuildable()` est 
rappelée.

Elle fonctionne avec deux `HashMap` :
- `Hashmap<String, ElementBuilder> builderMap` : cartographie les différents éléments constructibles
- `Hashmap<String, Buildable> elementsMap` : cartographie l'ensemble des éléments déjà construits grâce à leur nom

### Interface Buildable

Ce package implémente également l'interface `Buildable`. Cette dernière a pour but de permettre à `Builder` de créer 
différents types d'objets (`Composant` et `Circuit`). 

Elle implémente également la méthode `boolean estCompsant()`, qui vise à permettre de savoir si un objet est un 
composant ou non, lors de la création d'un circuit.

### Superclasse ElementBuilder

La superclasse `ElementBuilder` vise à fournir une base de méthodes pour créer des `Buildable` :
- la méthode `Buildable build(String[] command, HashMap<String, Buildable> mapElements)` qui peut soulever une `UnknownBuildableException`, revoie un composant ou un circuit
créé à partir de sa sous-classe spécifique.
  
Les sous-classes sont les suivantes :
- `AndBuilder` pour créer des portes et
- `OrBuilder` pour créer des portes ou
- `NotBuilder` pour créer des portes non
- `InterrupteurBuilder` pour créer des interrupteurs
- `VanneBuilder` pour créer des vannes
- `CircuitBuilder` pour créer un circuit à partir de la carte `mapElements`

## Simulation

Le package `Simulation` vise à fournir une interface de simulation par le biais de commandes. Il est basé sur la classe
`Simulateur` et sur la superclasse `Commande`.

### Classe Simulateur

La classe Simulateur vise à lire et exécuter des commandes. Elle utilise pour cela une `HashMap` qui répertorie les 
commandes acceptées accompagnées de leurs objets `Commande` associés.

### Superclasse Commande

La superclasse `Commande` fournit les trois méthodes suivantes :
- `abstract boolean execute(String[] args)` : exécute la commande et affiche son résultat
- `abstract String explication()` : renvoie le prototype de la commande
- `boolean messsageErreur(String message)` : affiche le message donné, suivi d'une aide sur l'utilisation de la comande

La méthode `execute` retourne un booléen, avec la valeur `true` par défaut, et `false` si la commande `exit` est entrée.

Les sous-classes implémentées correspondent à l'ensemble des fonctions intégrées (cf. partie n°1).

# III) Choix, difficultés

## Intégration d'un constructeur

Sur la base du TP sur les formes géométriques, j'ai souhaité intégrer un constructeur de circuit au projet. Pour cela, 
j'avais commencé par intégrer la méthode `build` directement dans les classes `Composant` et `Circuit`, avant de me 
rendre compte que la création de classes à part entière était un choix plus judicieux, du point de vue du paradigme  
objet.

De plus, j'ai eu du mal au départ à saisir le fonctionnement d'une Hashmap : j'ai en effet pensé que le second champ (
ici Buildable) contenait un message à envoyer et non un objet.

## Méthodes boolean est*

A diverses occasions, des méthodes de type `boolean est*()` (`boolean estEntree()`, `boolean estSortie()`, `boolean 
estComposant()`) sont créées. Leur utilité est la même que l'opérateur `instanceof`, mais cela a pour avantage de ne pas
dépendre du nom de la classe, suceptible de changer. De plus, la création d'une méthode spécifique est intéressante dans
le cadre de tests : on pensera plus facilement à tester une méthode qu'un code du type :
```
assertTrue((new Object()) instanceof Object);
```
dont le résultat est évident.

L'idée d'implémenter ce type de méthode me vient de mon expérience de développement avec l'API .NET, ou ce genre de 
méthodes sont courantes.